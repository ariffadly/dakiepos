<aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?php echo $_SESSION['gambar']; ?>" height="200" width="200" style="border: 2px solid white;" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><?php echo $_SESSION['fullname']; ?></p>
              <a href="index.php"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div><br />
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MENU UTAMA PENJUALAN</li>
            <li class="active treeview">
              <a href="index.php">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
              </a>
            </li>
            
            <li>
              <a href="#">
                <i class="fa fa-file"></i> <span>Produk</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="produk.php"><i class="fa fa-circle-o"></i> Produk</a></li>
                <li><a href="input-produk.php"><i class="fa fa-circle-o"></i> Tambah Produk</a></li>                
                <li><a href="stok.php"><i class="fa fa-circle-o"></i> Re-Stock Gudang</a></li>
                <li><a href="stok_reseller.php"><i class="fa fa-circle-o"></i> Re-Stock Cabang</a></li>
                <li><a href="restock.php"><i class="fa fa-circle-o"></i> Data Re-Stock</a></li>
                <li><a href="habis.php"><i class="fa fa-circle-o"></i> Data Stock Habis</a></li>
                <li><a href="produk_importxls.php"><i class="fa fa-circle-o"></i> Import Data Excel</a></li>
              </ul>
            </li>
            
<!--            <li>-->
<!--              <a href="#">-->
<!--                <i class="fa fa-th"></i> <span>Kategori</span> <i class="fa fa-angle-left pull-right"></i>-->
<!--              </a>-->
<!--              <ul class="treeview-menu">-->
<!--                <li><a href="kategori.php"><i class="fa fa-circle-o"></i> Kategori</a></li>-->
<!--                <li><a href="input-kategori.php"><i class="fa fa-circle-o"></i> Tambah Kategori</a></li>-->
<!--                <li><a href="kategori_importxls.php"><i class="fa fa-circle-o"></i> Import Data Excel</a></li>-->
<!--              </ul>-->
<!--            </li>-->
            
            <!--li class="treeview">
              <a href="#">
                <i class="fa fa-cube"></i> <span>Pembelian (Re-stock)</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="po.php"><i class="fa fa-circle-o"></i> Data PO</a></li>
                <li><a href="input-po.php"><i class="fa fa-circle-o"></i> Input PO</a></li>
              </ul>
            </li-->
            
            <li class="treeview">
              <a href="#">
                <i class="fa fa-dollar"></i> <span>Transaksi</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="transaksi.php"><i class="fa fa-circle-o"></i> Data Transaksi Penjualan</a></li>
                <li><a href="penjualan.php"><i class="fa fa-circle-o"></i> Penjualan</a></li>
              </ul>
            </li>
            
            <li>
              <a href="#">
                <i class="fa fa-user"></i> <span>Member</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="member.php"><i class="fa fa-circle-o"></i> Member</a></li>
                <li><a href="input-member.php"><i class="fa fa-circle-o"></i> Tambah Member</a></li>
                <li><a href="member_importxls.php"><i class="fa fa-circle-o"></i> Import Data Excel</a></li>
              </ul>
            </li>

            <li>
              <a href="#">
                <i class="fa fa-user"></i> <span>Lokasi</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="lokasi.php"><i class="fa fa-circle-o"></i> Lokasi</a></li>
                <li><a href="input-lokasi.php"><i class="fa fa-circle-o"></i> Tambah Lokasi</a></li>
                <!--li><a href="pilih-lokasi.php"><i class="fa fa-circle-o"></i> Edit Stok Produk Lokasi</a></li-->
              </ul>
            </li>
            
            <?php $tampil1=mysqli_query($koneksi, "select * from supplier order by id_sup desc");
                        $total1=mysqli_num_rows($tampil1);
                    ?>
            
            <!--li>
              <a href="#">
                <i class="fa fa-users"></i> <span>Merk</span>
                <small class="label pull-right bg-red"><?php echo $total1; ?></small>
              </a>
               <ul class="treeview-menu">
                <li><a href="supplier.php"><i class="fa fa-circle-o"></i> Merk</a></li>
                <li><a href="input-supplier.php"><i class="fa fa-circle-o"></i> Tambah Merk</a></li>
                <li><a href="supplier_importxls.php"><i class="fa fa-circle-o"></i> Import Data Excel</a></li>
              </ul>
            </li-->
            
            <?php $tampil=mysqli_query($koneksi, "select * from admin order by user_id desc");
                        $total=mysqli_num_rows($tampil);
                    ?>
            <li>
              <a href="#">
                <i class="fa fa-lock"></i> <span>Admin</span>
                <small class="label pull-right bg-yellow"><?php echo $total; ?></small>
              </a>
               <ul class="treeview-menu">
                <li><a href="admin.php"><i class="fa fa-circle-o"></i> Admin</a></li>
                <li><a href="input-admin.php"><i class="fa fa-circle-o"></i> Tambah Admin</a></li>
              </ul>
            </li>
            
            <li>
              <a href="#">
                <i class="fa fa-user"></i> <span>Laporan</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="laporan-penjualan.php"><i class="fa fa-circle-o"></i> Laporan Penjualan</a></li>
                <li><a href="laporan-terlaris.php"><i class="fa fa-circle-o"></i> Laporan Penjualan Terlaris</a></li>
                <li><a href="laporan-produk.php" target="_blank"><i class="fa fa-circle-o"></i> Laporan Data Produk</a></li>
                <!--li><a href="cetak-supplier.php" target="_blank"><i class="fa fa-circle-o"></i> Laporan Data Supplier</a></li-->
                <li><a href="cetak-member.php" target="_blank"><i class="fa fa-circle-o"></i> Laporan Data Member</a></li>
              </ul>
            </li>
        </section>
        <!-- /.sidebar -->
      </aside>