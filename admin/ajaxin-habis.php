<?php
/* Database connection start */
/*$servername = "localhost";
$username = "root";
$password = "";
$dbname = "penjualan";

$conn = mysqli_connect($servername, $username, $password, $dbname) or die("Connection failed: " . mysqli_connect_error());*/

/* Database connection end */


// storing  request (ie, get/post) global array to a variable  
include "../conn.php";

$requestData= $_REQUEST;


$columns = array( 
// datatable column index  => database column name
    0 => 'nama_produk', 
	1 => 'jenis_produk',
	2 => 'kategori',
    3 => 'nama',
    4 => 'alamat',
    5 => 'kd_produk'

);

// getting total number records without any search
$sql = "SELECT pc.kd_produk, p.nama_produk, k.jenis_produk, k.nama_kategori kategori, pc.stock, c.nama, c.alamat";
$sql.=" FROM produk p, kategori k, cabang c,produk_cabang pc where pc.stock<1 and pc.id_cabang=c.ID and pc.kd_produk=p.kd_produk and p.kategori=k.id";
$query=mysqli_query($koneksi, $sql) or die("ajaxin-grid-data.php: get Produk");
$totalData = mysqli_num_rows($query);
$totalFiltered = $totalData;  // when there is no search parameter then total number rows = total number filtered rows.


if( !empty($requestData['search']['value']) ) {
	// if there is a search parameter
	$sql = "SELECT pc.kd_produk, p.nama_produk, k.jenis_produk, k.nama_kategori kategori, pc.stock, c.nama, c.alamat";
	$sql.=" FROM produk p, kategori k, cabang c,produk_cabang pc where pc.stock<1 and pc.id_cabang=c.ID and pc.kd_produk=p.kd_produk and p.kategori=k.id";
	$sql.=" AND pc.kd_produk LIKE '%".$requestData['search']['value']."%' ";    // $requestData['search']['value'] contains search parameter
	$sql.=" OR pc.stock<1 and pc.id_cabang=c.ID and pc.kd_produk=p.kd_produk and p.kategori=k.id AND p.nama_produk LIKE '%".$requestData['search']['value']."%' ";
	$sql.=" OR pc.stock<1 and pc.id_cabang=c.ID and pc.kd_produk=p.kd_produk and p.kategori=k.id AND k.jenis_produk LIKE '".$requestData['search']['value']."%' ";
    $sql.=" OR pc.stock<1 and pc.id_cabang=c.ID and pc.kd_produk=p.kd_produk and p.kategori=k.id AND k.nama_kategori LIKE '".$requestData['search']['value']."%' ";
    $sql.=" OR pc.stock<1 and pc.id_cabang=c.ID and pc.kd_produk=p.kd_produk and p.kategori=k.id AND c.nama LIKE '".$requestData['search']['value']."%' ";
    $sql.=" OR pc.stock<1 and pc.id_cabang=c.ID and pc.kd_produk=p.kd_produk and p.kategori=k.id AND c.alamat LIKE '%".$requestData['search']['value']."%' ";
	$query=mysqli_query($koneksi, $sql) or die("ajax-grid-data.php: get PO");
	$totalFiltered = mysqli_num_rows($query); // when there is a search parameter then we have to modify total number filtered rows as per search result without limit in the query 

	$sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."   LIMIT ".$requestData['start']." ,".$requestData['length']."   "; // $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc , $requestData['start'] contains start row number ,$requestData['length'] contains limit length.
	$query=mysqli_query($koneksi, $sql) or die("ajaxin-grid-data.php: get PO"); // again run query with limit
	
} else {	

	$sql = "SELECT pc.kd_produk, p.nama_produk, k.jenis_produk, k.nama_kategori kategori, pc.stock, c.nama, c.alamat";
	$sql.=" FROM produk p, kategori k, cabang c,produk_cabang pc where pc.stock<1 and pc.id_cabang=c.ID and pc.kd_produk=p.kd_produk and p.kategori=k.id";
	$sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."   LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
	$query=mysqli_query($koneksi, $sql) or die("ajaxin-grid-data.php: get PO");   
	
}

$data = array();
while( $row=mysqli_fetch_array($query) ) {  // preparing an array
	$nestedData=array(); 

    $nestedData[] = $row["nama_produk"];
	$nestedData[] = $row["jenis_produk"];
	$nestedData[] = $row["kategori"];
    $nestedData[] = $row["nama"];
    $nestedData[] = $row["alamat"];
    $nestedData[] = $row["kd_produk"];

	$data[] = $nestedData;
    
}



$json_data = array(
			"draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
			"recordsTotal"    => intval( $totalData ),  // total number of records
			"recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data"            => $data   // total data array
			);

echo json_encode($json_data);  // send data as json format

?>
