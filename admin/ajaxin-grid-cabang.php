<?php
/* Database connection start */
/*$servername = "localhost";
$username = "root";
$password = "";
$dbname = "penjualan";

$koneksi = mysqli_connect($servername, $username, $password, $dbname) or die("Connection failed: " . mysqli_connect_error());

/* Database connection end */
session_start();
include "../conn.php";

// storing  request (ie, get/post) global array to a variable
$requestData= $_REQUEST;


$columns = array(
// datatable column index  => database column name
    0 => 'nama_produk',
    3 => 'stock',
    5 => 'kd_produk'
);

// getting total number records without any search
$sql = "SELECT p.kd_produk, p.nama_produk, c.stock";
$sql.=" FROM produk p, produk_cabang c where p.kd_produk=c.kd_produk and c.id_cabang=$_SESSION[id_cabang]";
$query=mysqli_query($koneksi, $sql) or die("ajaxin-grid-data.php: get Produk");
$totalData = mysqli_num_rows($query);
$totalFiltered = $totalData;  // when there is no search parameter then total number rows = total number filtered rows.


if( !empty($requestData['search']['value']) ) {
    // if there is a search parameter
    $sql = "SELECT p.kd_produk, p.nama_produk, c.stock";
    $sql.=" FROM produk p, produk_cabang c where p.kd_produk=c.kd_produk and c.id_cabang=$_SESSION[id_cabang]";
    $sql.=" AND p.kd_produk LIKE '%".$requestData['search']['value']."%' ";    // $requestData['search']['value'] contains search parameter
    $sql.=" OR p.kd_produk=c.kd_produk and c.id_cabang=$_SESSION[id_cabang] AND p.nama_produk LIKE '%".$requestData['search']['value']."%' ";
    $sql.=" OR p.kd_produk=c.kd_produk and c.id_cabang=$_SESSION[id_cabang] AND c.stock LIKE '".$requestData['search']['value']."%' ";
    $query=mysqli_query($koneksi, $sql) or die("ajax-grid-data.php: get PO");
    $totalFiltered = mysqli_num_rows($query); // when there is a search parameter then we have to modify total number filtered rows as per search result without limit in the query

    $sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."   LIMIT ".$requestData['start']." ,".$requestData['length']."   "; // $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc , $requestData['start'] contains start row number ,$requestData['length'] contains limit length.
    $query=mysqli_query($koneksi, $sql) or die("ajaxin-grid-data.php: get PO"); // again run query with limit

} else {

    $sql = "SELECT p.kd_produk, p.nama_produk, c.stock";
    $sql.=" FROM produk p, produk_cabang c where p.kd_produk=c.kd_produk and c.id_cabang=$_SESSION[id_cabang]";
    $sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."   LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
    $query=mysqli_query($koneksi, $sql) or die("ajaxin-grid-data.php: get PO");

}

$data = array();
while( $row=mysqli_fetch_array($query) ) {  // preparing an array
    $nestedData=array();

    $nestedData[] = $row["nama_produk"];
    $nestedData[] = $row["stock"];
    $nestedData[] = $row["kd_produk"];
    $nestedData[] = '<td><center>
                     <a href="cetak-barcode.php?id='.$row['kd_produk'].'"  data-toggle="tooltip" title="Cetak Barcode" class="btn btn-sm btn-info"> <i class="glyphicon glyphicon-print"></i> </a>
                     <a href="detail-produk.php?id='.$row['kd_produk'].'"  data-toggle="tooltip" title="View Detail" class="btn btn-sm btn-success"> <i class="glyphicon glyphicon-search"></i> </a>
	                 </center></td>';

    $data[] = $nestedData;

}



$json_data = array(
    "draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
    "recordsTotal"    => intval( $totalData ),  // total number of records
    "recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
    "data"            => $data   // total data array
);

echo json_encode($json_data);  // send data as json format

?>
