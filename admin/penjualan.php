<?php 
session_start();
if (empty($_SESSION['username'])){
	header('location:../index.php');	
} else {
	include "../conn.php";

date_default_timezone_set("Asia/Bangkok");
date_default_timezone_get();
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>POS (Point Of Sales) V 1.0</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../css/font-awesome.min.css">
    <link rel="stylesheet" href="../css/jquery-ui.min.css">
    <!-- Ionicons -->
    
    <link rel="stylesheet" href="../plugins/iCheck/all.css">
    
    <link rel="stylesheet" href="../css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="../plugins/iCheck/flat/blue.css">
    <!-- Morris chart -->
    <link rel="stylesheet" href="../plugins/morris/morris.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="../plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="../plugins/datepicker/datepicker3.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="../plugins/daterangepicker/daterangepicker-bs3.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <!-- css table datatables/dataTables -->
	<link rel="stylesheet" href="../plugins/datatables/dataTables.bootstrap.css"/>
    
    <link rel="stylesheet" href="../plugins/select2/select2.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <?php include "header.php"; ?>
      <!-- Left side column. contains the logo and sidebar -->
      <?php include "menu.php"; ?>

<?php
/*$timeout = 120; // Set timeout minutes
$logout_redirect_url = "../index.php"; // Set logout URL
*/
?>
<?php } ?>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Penjualan
            <small>Point Of Sales</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Penjualan</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Main row -->
          <div class="row">
            <!-- Left col -->
            <section class="col-lg-12 connectedSortable">

              <!-- TO DO List -->
              <div class="box box-primary">
                <div class="box-header">
                  <i class="ion ion-clipboard"></i>
                  <h3 class="box-title">Transaksi Penjualan</h3>
                  <div class="box-tools pull-right">
                  </div> 
                </div><!-- /.box-header -->
                
                <div class="box-body">
                <?php
                // 'hal']) == 'hapus'){
             if(isset($_GET['kd']) && isset($_GET['qty'])){
				$id = $_GET['kd'];
                $qty1 = $_GET['qty'];
				$cek = mysqli_query($koneksi, "SELECT * FROM transaksi_temporary WHERE kd_produk='$id' AND session='$_SESSION[user_id]' AND id_cabang='$_SESSION[id_cabang]'");
				if(mysqli_num_rows($cek) == 0){
					echo "<script>window.location = 'penjualan.php'</script>";
                    //echo '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> Data tidak ditemukan.</div>';
				}else{
					$delete = mysqli_query($koneksi, "DELETE FROM transaksi_temporary WHERE kd_produk='$id'");
                    
                    $que			= mysqli_query($koneksi, "UPDATE produk SET stock=(stock+'$qty1') WHERE kd_produk='$id' AND id_cabang='$_SESSION[id_cabang]'");
					if($delete&&$que){
					echo "<script>window.location = 'penjualan.php'</script>";
                    	//echo '<div class="alert alert-primary alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> Data berhasil dihapus.</div>';
					}else{
						//echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> Data gagal dihapus.</div>';
					echo "<script>window.location = 'penjualan.php'</script>";
                    }
				}
			}
			?>
            
            <?php
            //input po transit dan tampil ke tabel
            if(isset($_POST['input'])){
                $tanggal = date("Y/m/d");
                $qty = $_POST['qty'];
                $kd = $_POST['kode'];
                $no_trans = $_POST['no_trans'];
                if(is_numeric($kd))  $sql = mysqli_query($koneksi, "SELECT * FROM produk p WHERE kd_produk='$kd'");
                else $sql = mysqli_query($koneksi, "SELECT * FROM produk WHERE nama_produk='$kd'");
             if(mysqli_num_rows($sql) == 0){
				header("Location: penjualan.php");
			}else{
				$row = mysqli_fetch_assoc($sql);
			}
            
                $nama_produk = $row['nama_produk'];
                $harga_beli  = $row['harga_beli'];
                $harga_jual  = $row['harga_jual'];
                $total       = $qty * $harga_jual;
                $tot         = $qty * $harga_beli;
                $profit      = $total - $tot;
                $stock       = $row['stock'];
                $kd_produk   = $row['kd_produk'];
                
        $cekno= mysqli_query($koneksi, "SELECT * FROM transaksi ORDER BY id DESC");
        
        
        $data1=mysqli_num_rows($cekno);
        $cekQ1=$data1;
        //$data=mysqli_fetch_array($ceknomor);
        //$cekQ=$data['f_kodepart'];
        #menghilangkan huruf
        //$awalQ=substr($cekQ,0-6);
        #ketemu angka awal(angka sebelumnya) + dengan 1
        $next1=$cekQ1+1;

        #menhitung jumlah karakter
        $kode1=strlen($next1);
        $p = date("Y-m-d");
        if(!$cekQ1)
        { $no1='000000'; }
        elseif($kode1==1)
        { $no1='000000'; }
        elseif($kode1==2)
        { $no1='00000'; }
        elseif($kode1==3)
        { $no1='0000'; }
        elseif($kode1==4)
        { $no1='000'; }
        elseif($kode1==5)
        { $no1='00'; }
        elseif($kode1=6)
        { $no1='0'; }
        elseif($kode1=7)
        { $no1=''; }

        $sesid=$_SESSION[user_id];
        // masukkan dalam tabel penjualan
        $kode=$p.$sesid.$no1.$next1;
        
				if ($stock == 0) {
		          echo "<script>alert('Stock Habis, Harap Cek dan Update Stock Dulu!'); window.location = 'penjualan.php'</script>";
	                } else {
                $ck = mysqli_query($koneksi, "SELECT * FROM transaksi_temporary where kd_produk='$kd_produk' and session='$_SESSION[user_id]' and id_cabang='$_SESSION[id_cabang]'");
                if(mysqli_num_rows($ck) == 0){

						$insert = mysqli_query($koneksi, "INSERT INTO transaksi_temporary(no_trans, tanggal, kd_produk, nama_produk, harga_beli, harga_jual, qty, total, profit, session,id_cabang)
						                                  VALUES('$kode', '$tanggal', '$kd_produk', '$nama_produk', '$harga_beli', '$harga_jual', '$qty', '$total', '$profit', '$_SESSION[user_id]','$_SESSION[id_cabang]')") or die(mysqli_error());
						}else{
            $insert = mysqli_query($koneksi, "UPDATE transaksi_temporary set qty=qty+$qty, total=total+$total, profit=profit+$profit where kd_produk='$kd_produk' and session='$_SESSION[user_id]' and id_cabang='$_SESSION[id_cabang]'") or die(mysqli_error());
            }
                        $qu			= mysqli_query($koneksi, "UPDATE produk SET stock=(stock-'$qty') WHERE kd_produk='$kd_produk'");
                        if($insert && $qu){
						         echo "<script>window.location = 'penjualan.php'</script>";   
                                    //}
                        }else{
							echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Ups, Data Gagal Di simpan !</div>';
						}
                    }
			}
            ?>
   
           <?php
            $query5 = mysqli_query($koneksi, "SELECT * FROM transaksi_temporary WHERE session='$_SESSION[user_id]' and id_cabang='$_SESSION[id_cabang]'");
            $data5  = mysqli_fetch_array($query5);
            ?>


                    
            <form id="form1" name="form1" method="post" action="">
            <div class="col-md-3">
            <div class="form-group">
                    <label></label>
                    <input type="text" class="form-control" name="no_trans" id="no_trans" value="<?php echo $data5['no_trans']; ?>" placeholder="No Transaksi" autocomplete="off" readonly="readonly"/>
                  </div><!-- /.form-group -->
                  </div>

                  <div class="col-md-1">
                  <div class="form-group">
                    <label></label>
                    <input type="text" class="form-control" name="qty" id="qty" value="1" autocomplete="off" required="required"  />
                  </div><!-- /.form-group -->
                  </div>
            <div class="col-md-6">
                  <div >
                    <label></label>
                   <input type="text" name="kode" class="form-control" id="kode" onkeypress="onEnter(event);" autofocus="true">
                  </div><!-- /.form-group -->
                  </div>
                  <div class="col-md-1">
                         
                  <div class="form-group">
                    <label></label>
                    <input type="submit" class="btn btn btn-primary" name="input" id="inpu" value="Tambah" autocomplete="off" required="required"/>
                  </div><!-- /.form-group -->
                  </div>
            </form>
		 
            <br />
            <?php
                    $query1="select * from transaksi_temporary where session='$_SESSION[user_id]' and id_cabang='$_SESSION[id_cabang]'";
                    
                    $tampil=mysqli_query($koneksi, $query1) or die(mysqli_error());
                    ?>
        <div class="table table-responsive">
            <form id="formku" name="formku" method="post">
		<table name="table1" class="table table-hover table-bordered" id="table1">  
			<thead bgcolor="eeeeee" align="center">
            <tr>
            <th>No</th>
            <th>Tanggal</th>
            <th>Kode</th>
            <th>Produk</th>
            <th hidden="true">Harga Beli</th>
            <th>Harga</th>
            <th>Qty</th>
            <th>Total</th>
            <th hidden="true">Profit</th>
            <th>Remove</th><tr>
			</thead>
            <?php 
                     
                     while($data=mysqli_fetch_array($tampil))
                    { ?>
                    <tbody>
                    <tr>
                    <td><center><?php echo $data['no_trans'];; ?></center></td>
                    <td><center><?php echo $data['tanggal'];?></center></td>
                    <td><center><?php echo $data['kd_produk'];?></center></td>
                    <td><center><?php echo $data['nama_produk'];?></center></td>
                    <td hidden="true"><center>Rp. <?php echo number_format($data['harga_beli'],0,",",".");?></center></td>
                    <td><center>Rp. <?php echo number_format($data['harga_jual'],0,",",".");?></center></td>
                    <td><center><?php echo $data['qty'];?></center></td>
                    <td><center>Rp. <?php echo number_format($data['total'],0,",",".");?></center></td>
                    <td hidden="true"><center>Rp. <?php echo number_format($data['profit'],0,",",".");?></center></td>
                    <td><center><div id="thanks"><a class="btn btn-sm btn-danger tooltips" data-placement="bottom" data-toggle="tooltip" title="Hapus List" href="penjualan.php?kd=<?php echo $data['kd_produk'];?>&&qty=<?php echo $data['qty'];?>"><span class="glyphicon glyphicon-trash"></a></center></td></tr></div>
                 <!--<td><center><div id="thanks"><a class="btn btn-sm btn-danger tooltips" data-placement="bottom" data-toggle="tooltip" title="Hapus List" href="penjualan.php?hal=hapus&&kd=<?php //echo $data['kd_produk'];?>&&qty=<?php //echo $data['qty'];?>"><span class="glyphicon glyphicon-trash"></a></center></td></tr></div>
                 -->
                 <?php   
                 $a=$data['tanggal'];
                 $b=$data['kd_produk'];
                 $c=$data['nama_produk'];
                 $d=$data['harga_beli'];
                 $e=$data['harga_jual'];
                 $f=$data['qty'];
                 $g=$data['total'];
                 $h=$data['profit'];
                 $i=$data['no_trans'];
  
                
if(isset($_GET['cancel'])){
  mysqli_query($koneksi,"UPDATE produk SET stock=stock+$f WHERE kd_produk='$b'");
mysqli_query($koneksi,"DELETE FROM transaksi_temporary");
echo "<script> window.location = 'penjualan.php' </script>";
}

                if(isset($_POST['simpanpo'])){
                $bayar       = $_POST['bayar'];
                $kembali     = $_POST['kembali'];
                $lokasi = $_POST['lokasi'];
                $acc    = $_POST['acc'];
				$tanggal1	 = $a;
				$kd_produk1	 = $b;
				$nama_produk1= $c;
				$harga_beli1 = $d;
				$harga_jual1 = $e;
                $qty1        = $f;
                $total1      = $g;
                $profit1     = $h;
                $no_trans1   = $i;
                //echo "<script>alert('$acc');</script>";
if($acc=="true"){
   
						$insert = mysqli_query($koneksi, "INSERT INTO transaksi(no_trans, tanggal_trans, total, profit,id_cabang)
						                                  VALUES('$no_trans1','$tanggal1','$total1','$profit1',$lokasi);") or die(mysqli_error());
						if($insert){
						            //$cek2 = mysqli_query($koneksi, "SELECT * FROM t_po_detail WHERE f_pono='$pono'");
                                    //if(mysqli_num_rows($cek2) == 0){
                                    $input = mysqli_query($koneksi, "INSERT INTO detail_transaksi(no_trans, kd_produk, nama_produk, qty) SELECT no_trans, kd_produk, nama_produk, qty FROM transaksi_temporary WHERE session='$_SESSION[user_id]' AND id_cabang='$_SESSION[id_cabang]'") or die(mysqli_error());
                                                            
                                    $delete = mysqli_query($koneksi, "DELETE FROM transaksi_temporary WHERE session='$_SESSION[user_id]' AND id_cabang='$_SESSION[id_cabang]'");
					                      //}
                                          //session _unset();
                                          //session_destroy();
      $session= $_SESSION[username];

//                              echo "<script>
//                              window.location = 'test1.php?kembali=$kembali&&bayar=$bayar&&session=$session&&no_trans=$no_trans1';
//
//                              </script>";

                                 
                                    //}
                        }else{
							echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Ups, Data Penjualan Gagal Di simpan !</div>';
						}
						
}else{
    echo "<script>window.location = 'penjualan.php';</script>";
}
				//}else{
				//	echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>No PO Sudah Ada..!</div>';
				//}
                
			} ?>
            <?php  } 
              ?>
              <thead bgcolor="eeeeee" align="center">
            <tr>
            <th colspan="4"></th>
            <th><center>Total</center></th>
            
            <th>
            <?php $sql1 = mysqli_query($koneksi, "SELECT SUM(qty) AS jum FROM transaksi_temporary WHERE session='$_SESSION[user_id]' AND id_cabang='$_SESSION[id_cabang]'");
             if(mysqli_num_rows($sql1) == 0){
				header("Location: penjualan.php");
			}else{
				$row1 = mysqli_fetch_assoc($sql1);  
			} 
            ?>
            <center><?php echo $row1['jum']; 
            $tota = $row1['jum'];
            ?></center>
            
            </th>
            <th> 
            <?php $sql2 = mysqli_query($koneksi, "SELECT SUM(total) AS tot FROM transaksi_temporary WHERE session='$_SESSION[user_id]' AND id_cabang='$_SESSION[id_cabang]'");
             if(mysqli_num_rows($sql2) == 0){
				header("Location: penjualan.php");
			}else{
				$row2 = mysqli_fetch_assoc($sql2);
			} 
            ?>
            <center><font style="color : blue;">Rp. <?php echo number_format($row2['tot'],0,",","."); ?>
            <input type="hidden" size="5" class="form-control" id="total" name="total" value="<?php echo $row2['tot']; ?>"/>
            </font></center>
            </th>
            <th hidden="true">
<?php $sql3 = mysqli_query($koneksi, "SELECT SUM(profit) AS pro FROM transaksi_temporary WHERE session='$_SESSION[user_id]' AND id_cabang='$_SESSION[id_cabang]'");
             if(mysqli_num_rows($sql3) == 0){
				header("Location: penjualan.php");
			}else{
				$row3 = mysqli_fetch_assoc($sql3);
			} 
            ?>
            <center><font style="color : green;">Rp. <?php echo number_format($row3['pro'],0,",",".");
            $prof = $row3['pro'];
             ?></font></center>
            </th>
            <th></th></tr>
            <tr>
            <th colspan="4"></th>
            <th><center>Bayar</center></th>
            <th></th>
            <th><input type="text" colspan="2" size="8" class="form-control" id="bayar" name="bayar" onkeyup="hitung()" onkeydown="hitung()" onchange="hitung()" autocomplete="off" required="required" /></th>
            <th></th>
            <th></th>
            </tr>
            <tr>
            <th colspan="4"></th>
            <th ><center>Kembali</center></th>
            <th></th>
            <th><input type="text" size="8" colspan="2" class="form-control" id="kembali" name="kembali" readonly="readonly" /></th>
            <th></th>
            <th></th>
            </tr>
            <tr>
            <th colspan="4"></th>
            <th ><center>Lokasi</center></th>
            <th></th>
            <th> <select id="lokasi" name="lokasi" class="form-control" required>
                              <option value="kosong">--Pilih Lokasi Penjualan--</option>
                              <?php
                    $query1="select * from cabang order by ID";
                    $tampil=mysqli_query($koneksi, $query1) or die(mysqli_error());
                    while($data=mysqli_fetch_array($tampil))
                    {
                    ?>
                              
                                  
              
              <option focus value="<?php echo $data['ID']; ?>"><?php echo $data['nama'];?></option>
                <?php } ?>
                              </select></th>
            <th></th>
            <th></th>
            </tr>
			</thead>
            </table>
            </div>
            <input type="hidden" id="acc" name="acc"/>
            	<div class="col-md-0">
					<button id="simpanpo" name="simpanpo" class="btn btn-success" onclick="cek();"><i class="fam-page-save"></i> Save Sales  </button>
            </form>&nbsp;&nbsp;&nbsp;<button id="cancel" class="btn btn-danger" onclick="gagal();"><i class="fam-page-save"></i>Cancel</button>
				</div>
        
      </div>

            </section><!-- /.Left col -->
          </div><!-- /.row (main row) -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      
      
      <?php include "footer.php"; ?>

      <?php include "sidecontrol.php"; ?>
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    <script src="../plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <script src="../plugins/jQuery/utilities.js"></script>
    <script src="../plugins/jQueryUI/jquery-ui.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="../bootstrap/js/bootstrap.min.js"></script>
    <!-- DataTables -->
    <script src="../plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="../plugins/datatables/dataTables.bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="../plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="../plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../dist/js/demo.js"></script>
    <script src="../plugins/select2/select2.full.min.js"></script>
	  <!--<script type="text/javascript"> 

            $(function () {
                $("#lookup").dataTable({"lengthMenu":[25,50,75,100],"pageLength":25});
            });
  
   
        </script>-->
        <script type="text/javascript">
    function hitung() {
var total = document.formku.total.value;
var bayar = document.formku.bayar.value;
var kembali = document.formku.kembali.value;

kembali =  (bayar - total ) ;
document.formku.kembali.value = Math.floor( kembali);

}

   function cek() {
var total = document.formku.total.value;
var bayar = document.formku.bayar.value;
var kembali = document.formku.kembali.value;

if(kembali<0 || !kembali){
  event.preventDefault();
  alert("uang kurang");
  window.location = 'penjualan.php';
}

var lokasi = document.formku.lokasi;
var selectedLokasi = lokasi.options[lokasi.selectedIndex].value;
if (selectedLokasi == "kosong")
       {
           alert("Pilih Lokasi Penjualan");
       }
       console.log("ada");
if(kembali >=0 && selectedLokasi != "kosong") konfirm();
}

function gagal(){
  var c=confirm("Yakin membatalkan transaksi ini?");
    if(c == true){
      window.location = 'penjualan.php?cancel=true';  
    }
}

function onEnter(e){
  var key=e.keyCode || e.which;
  
  if(key==13){

    //document.getElementById("qty").value=1;

  event.preventDefault();

setTimeout(function() {
    document.getElementById("inpu").click();
}, 1000);
                              
    
  }
/*  function submitForm(){
    document.form1.submit();
    document.form1.method='post';
}*/
}
</script>
        
       
 <script>
        $(document).ready(function() {
				var dataTable = $('#lookup').DataTable( {
					"processing": true,
					"serverSide": true,
					"ajax":{
						url :"ajaxin-grid-data.php", // json datasource
						type: "post",  // method  , by default get
						error: function(){  // error handling
							$(".lookup-error").html("");
							$("#lookup").append('<tbody class="employee-grid-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');
							$("#lookup_processing").css("display","none");
							
						}
					}
				} );
			} );
        </script>
        <script type="text/javascript">
$(function() 
{
 $("#kode").autocomplete({
  source: 'autocomplete.php'
 });
});

function konfirm(){
    var total = document.getElementById("total").value;
    var bayar = document.getElementById("bayar").value;
    var kembali = document.getElementById("kembali").value;
    var r=confirm("Total = "+total+"\nBayar = "+bayar+"\nKembali = "+kembali);
    if(r == true){
      document.getElementById("acc").value = "true";
        //alert(document.getElementById("acc").value);
      //document.getElementById("acc").value = "true";  
    }  
}
</script>
  </body>
</html>
