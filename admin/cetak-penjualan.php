<?php
 //Define relative path from this script to mPDF
 $nama_file='Penjualan'; //Beri nama file PDF hasil.
define('_MPDF_PATH','../mpdf60/');
date_default_timezone_set("Asia/Bangkok");
date_default_timezone_get();
//define("_JPGRAPH_PATH", '../mpdf60/graph_cache/src/');

//define("_JPGRAPH_PATH", '../jpgraph/src/'); 
 
include(_MPDF_PATH . "mpdf.php");
//include(_MPDF_PATH . "graph.php");

//include(_MPDF_PATH . "graph_cache/src/");

$mpdf=new mPDF('utf-8', 'A4'); // Create new mPDF Document
 
//Beginning Buffer to save PHP variables and HTML tags
ob_start(); 

$mpdf->useGraphs = true;

?>
<!DOCTYPE html>
<html>
<head>
    <title>Laporan Penjualan Front Coverr</title>
    <style>
        *
        {
            margin:0;
            padding:0;
            font-family: calibri;
            font-size:12pt;
            color:#000;
        }
        body
        {
            width:100%;
            font-family: calibri;
            font-size:10pt;
            margin:0;
            padding:0;
        }
         
        p
        {
            margin:0;
            padding:0;
            margin-left: 200px;
        }
         
        #wrapper
        {
            width:200mm;
            margin:0 5mm;
        }
         
        .page
        {
            height:297mm;
            width:210mm;
            page-break-after:always;
        }
 
        table
        {
            border-left: 1px solid #fff;
            border-top: 1px solid #fff;
            font-family: calibri; 
            border-spacing:0;
            border-collapse: collapse; 
             
        }
         
        table td 
        {
            border-right: 1px solid #fff;
            border-bottom: 1px solid #fff;
            padding: 2mm;
            
        }
         
        table.heading
        {
            height:50mm;
        }
         
        h1.heading
        {
            font-size:12pt;
            color:#000;
            font-weight:normal;
            font-style: italic;
        }
         
        h2.heading
        {
            font-size:12pt;
            color:#000;
            font-weight:normal;
        }
         
        hr
        {
            color:#ccc;
            background:#ccc;
        }
         
        #invoice_body
        {
            height: auto;
        }
         
        #invoice_body , #invoice_total
        {   
            width:100%;
        }
        #invoice_body table , #invoice_total table
        {
            width:100%;
            border-left: 1px solid #ccc;
            border-top: 1px solid #ccc;
     
            border-spacing:0;
            border-collapse: collapse; 
             
            margin-top:5mm;
        }
         
        #invoice_body table td , #invoice_total table td
        {
            text-align:center;
            font-size:10pt;
            border-right: 1px solid black;
            border-bottom: 1px solid black;
            padding:2mm 0;
        }
         
        #invoice_body table td.mono  , #invoice_total table td.mono
        {
            text-align:right;
            padding-right:3mm;
            font-size:10pt;
        }
         
        #footer
        {   
            width:200mm;
            margin:0 5mm;
            padding-bottom:3mm;
        }
        #footer table
        {
            width:100%;
            border-left: 1px solid #ccc;
            border-top: 1px solid #ccc;
             
            background:#eee;
             
            border-spacing:0;
            border-collapse: collapse; 
        }
        #footer table td
        {
            width:25%;
            text-align:center;
            font-size:11pt;
            border-right: 1px solid #ccc;
            border-bottom: 1px solid #ccc;
        }
    </style>
</head>
<body>
<div id="wrapper">
     <?php
     include "../conn.php";
     ?>
   
    <table class="heading" style="width:100%;">

        <tr>
        <td> <center><p style="text-align:center; font-size: 18px; font-weight:bold;">LAPORAN PENJUALAN</p></center></td>
        </tr>
        <tr>
        <td> <center><p style="text-align:center; font-size: 14px; font-weight:bold;"></p></p></center></td>
        </tr>
    </table><br />
         <table>
         <tr>
        <td><td><p style="text-align:left; font-size: 12px; font-weight:bold;">Dari Tanggal : <?php echo $_GET['kd']; ?> </p></td></td>
        <td><td><p style="text-align:left; font-size: 12px; font-weight:bold;">Sampai Tanggal : <?php echo $_GET['kode']; ?> </p></td></td>
        </tr>
         </table>
         
    <div id="content">
         
        <div id="invoice_body">
        <?php
            $kd=$_GET['kd'];
            $kode=$_GET['kode'];
            $idlok=$_GET['idlok'];
            //$query1="SELECT * FROM transaksi WHERE (tanggal_trans between '$kd' and '$kode')";
            if($idlok>0){
            $query1="SELECT distinct d.no_trans,t.tanggal_trans,p.nama_produk,p.harga_jual,d.qty,(p.harga_jual*d.qty) total,((p.harga_jual-p.harga_beli)*d.qty) profit,c.nama FROM `detail_transaksi` d,transaksi t,produk p,cabang c where c.ID=$idlok and (t.tanggal_trans between '$kd' and '$kode') and d.no_trans=t.no_trans and d.kd_produk=p.kd_produk and t.id_cabang=c.ID";
            }else{
            $query1="SELECT distinct d.no_trans,t.tanggal_trans,p.nama_produk,p.harga_jual,d.qty,(p.harga_jual*d.qty) total,((p.harga_jual-p.harga_beli)*d.qty) profit,c.nama FROM `detail_transaksi` d,transaksi t,produk p,cabang c where (t.tanggal_trans between '$kd' and '$kode') and d.no_trans=t.no_trans and d.kd_produk=p.kd_produk and t.id_cabang=c.ID";
            }
            $tampil=mysqli_query($koneksi, $query1) or die(mysqli_error());
            ?>
            <table border="1">
            
            <tr>
                <td style="width:5%;"><b>No</b></td>
                <td style="width:25%;"><b>No Transaksi</b></td>
                <td style="width:15%;"><b>Tanggal</b></td>
                <td style="width:25%;"><b>Nama Produk</b></td>
                <td style="width:15%;"><b>Harga Jual</b></td>
                <td style="width:10%;"><b>Terjual</b></td>
                <td style="width:15%;"><b>Total</b></td>
                <td style="width:15%;"><b>Profit</b></td>
                <td style="width:15%;"><b>Lokasi</b></td>
            </tr>
            <?php
            $no=0;
            $total=0;
            $temp="";
            $totaljual=0;
                     while($data1=mysqli_fetch_array($tampil))
                    { $trans=$data1['no_trans'];
                      $bl = false;
                      if($temp<>$trans){
                      $no++;
                      $temp=$trans;
                      $bl=true;
                    }
                    $total=$total+$data1['profit'];
                    $totaljual=$totaljual+$data1['total'];
                    
                      /*$lokasi="";
                      $idcab=$data['id_cabang'];
                      if($idcab>0){
                        $cab=mysqli_query($koneksi,"SELECT * from cabang where ID=$idcab");
                        while($cb=mysqli_fetch_array($cab)){
                          $lokasi=$cb['nama'];
                        }
                      }*/
                        ?>
            <tr>
            <?php if($bl){ ?>
                <td style="width:10%;" class="mono"><b><?php echo $no; ?></b></td>
                <td style="width:10%;" class="mono"><b><?php echo $data1['no_trans']; ?></b></td>
                <td style="width:10%;" class="mono"><b><?php echo $data1['tanggal_trans']; ?></b></td>
                <?php } else{ ?>
                    <td></td>
                    <td></td>
                    <td></td>
                    <?php } ?>
                <td style="width:10%; text-align:left; padding-left: 3px;"><b><?php echo $data1['nama_produk']; ?></b></td>
                <td style="width:10%;" class="mono"><b><?php echo $data1['harga_jual']; ?></b></td>
                <td style="width:10%;" class="mono"><b><?php echo $data1['qty']; ?></b></td>
                <td style="width:10%;" class="mono"><b>Rp. <?php echo number_format($data1['total'],0,",","."); ?></b></td>
                <td style="width:10%;" class="mono"><b>Rp. <?php echo number_format($data1['profit'],0,",","."); ?></b></td>
            
                <td style="width:10%;" class="mono"><b><?php echo $data1['nama']; ?></b></td>
            </tr>         

             <?php   
              } 
              ?>
              <tr>
              <td colspan="9" style="width:10%;" class="mono"><b>Total Penjualan : Rp. <?php echo number_format($totaljual,0,",","."); ?></b></td>
            </tr>         
              <tr>
              <td colspan="9" style="width:10%;" class="mono"><b>Total Pendapatan : Rp. <?php echo number_format($total,0,",","."); ?></b></td>
            </tr>         
        </table>
        </div>
    </div>
    <br />
    </div>
     
     <?php

$html = ob_get_contents(); //Proses untuk mengambil data
ob_end_clean();
//Here convert the encode for UTF-8, if you prefer the ISO-8859-1 just change for $mpdf->WriteHTML($html);
$mpdf->WriteHTML(utf8_encode($html));
// LOAD a stylesheet
$stylesheet = file_get_contents('mpdfstyletables.css');
$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

$mpdf->WriteHTML($html,1);

$mpdf->Output($nama_file."-".date("Y/m/d H:i:s").".pdf" ,'I');

 


exit; 
?>
</body>
</html>