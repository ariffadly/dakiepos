<?php
 //Define relative path from this script to mPDF
 $nama_file='StokHabis'; //Beri nama file PDF hasil.
define('_MPDF_PATH','../mpdf60/');
date_default_timezone_set("Asia/Bangkok");
date_default_timezone_get();
//define("_JPGRAPH_PATH", '../mpdf60/graph_cache/src/');

//define("_JPGRAPH_PATH", '../jpgraph/src/'); 
 
include(_MPDF_PATH . "mpdf.php");
//include(_MPDF_PATH . "graph.php");

//include(_MPDF_PATH . "graph_cache/src/");

$mpdf=new mPDF('utf-8', 'A4'); // Create new mPDF Document
 
//Beginning Buffer to save PHP variables and HTML tags
ob_start(); 

$mpdf->useGraphs = true;

?>
<!DOCTYPE html>
<html>
<head>
    <title>Laporan Stok Habis Front Coverr</title>
    <style>
        *
        {
            margin:0;
            padding:0;
            font-family: calibri;
            font-size:12pt;
            color:#000;
        }
        body
        {
            width:100%;
            font-family: calibri;
            font-size:10pt;
            margin:0;
            padding:0;
        }
         
        p
        {
            margin:0;
            padding:0;
            margin-left: 200px;
        }
         
        #wrapper
        {
            width:200mm;
            margin:0 5mm;
        }
         
        .page
        {
            height:297mm;
            width:210mm;
            page-break-after:always;
        }
 
        table
        {
            border-left: 1px solid #fff;
            border-top: 1px solid #fff;
            font-family: calibri; 
            border-spacing:0;
            border-collapse: collapse; 
             
        }
         
        table td 
        {
            border-right: 1px solid #fff;
            border-bottom: 1px solid #fff;
            padding: 2mm;
            
        }
         
        table.heading
        {
            height:50mm;
        }
         
        h1.heading
        {
            font-size:12pt;
            color:#000;
            font-weight:normal;
            font-style: italic;
        }
         
        h2.heading
        {
            font-size:12pt;
            color:#000;
            font-weight:normal;
        }
         
        hr
        {
            color:#ccc;
            background:#ccc;
        }
         
        #invoice_body
        {
            height: auto;
        }
         
        #invoice_body , #invoice_total
        {   
            width:100%;
        }
        #invoice_body table , #invoice_total table
        {
            width:100%;
            border-left: 1px solid #ccc;
            border-top: 1px solid #ccc;
     
            border-spacing:0;
            border-collapse: collapse; 
             
            margin-top:5mm;
        }
         
        #invoice_body table td , #invoice_total table td
        {
            text-align:center;
            font-size:10pt;
            border-right: 1px solid black;
            border-bottom: 1px solid black;
            padding:2mm 0;
        }
         
        #invoice_body table td.mono  , #invoice_total table td.mono
        {
            text-align:right;
            padding-right:3mm;
            font-size:10pt;
        }
         
        #footer
        {   
            width:200mm;
            margin:0 5mm;
            padding-bottom:3mm;
        }
        #footer table
        {
            width:100%;
            border-left: 1px solid #ccc;
            border-top: 1px solid #ccc;
             
            background:#eee;
             
            border-spacing:0;
            border-collapse: collapse; 
        }
        #footer table td
        {
            width:25%;
            text-align:center;
            font-size:11pt;
            border-right: 1px solid #ccc;
            border-bottom: 1px solid #ccc;
        }
    </style>
</head>
<body>
<div id="wrapper">
     <?php
     include "../conn.php";
	 $lokasi=$_POST['lokasi'];
     ?>
   
    <table class="heading" style="width:100%;">

        <tr>
        <td> <center><p style="text-align:center; font-size: 18px; font-weight:bold;">LAPORAN STOK HABIS</p></center></td>
        </tr>
        <tr>
        <td> <center><p style="text-align:center; font-size: 14px; font-weight:bold;"></p></p></center></td>
        </tr>
    </table><br />
         
    <div id="content">
         
        <div id="invoice_body">
        <?php
            //$query1="SELECT * FROM transaksi WHERE (tanggal_trans between '$kd' and '$kode')";
			if($lokasi==0){
				
            $query1="SELECT p.kd_produk, p.nama_produk, k.jenis_produk, k.nama_kategori kategori, c.nama, c.alamat 
				FROM produk p, kategori k, supplier s,cabang c,produk_cabang pc where pc.kd_produk=p.kd_produk and pc.id_cabang=c.ID and p.kategori=k.id and p.supplier=s.id_sup
				ORDER BY nama,nama_produk ASC";
			}else{
				           $query1="SELECT p.kd_produk, p.nama_produk, k.jenis_produk, k.nama_kategori kategori, c.nama, c.alamat 
				FROM produk p, kategori k, supplier s,cabang c,produk_cabang pc where c.ID=$lokasi and pc.kd_produk=p.kd_produk and pc.id_cabang=c.ID and p.kategori=k.id and p.supplier=s.id_sup
				ORDER BY nama,nama_produk ASC";
			}
        
            $tampil=mysqli_query($koneksi, $query1) or die(mysqli_error());
            ?>
            <table border="1">
            
            <tr>
               <th>No</th>
			   <th>Kode</th>
			   <th>Nama Produk</th>
               <th>Nama Tempat</th>
               <th>Alamat</th>
            </tr>
            <?php
            $no=1;
                     while($data1=mysqli_fetch_array($tampil))
                    {                     
                      /*$lokasi="";
                      $idcab=$data['id_cabang'];
                      if($idcab>0){
                        $cab=mysqli_query($koneksi,"SELECT * from cabang where ID=$idcab");
                        while($cb=mysqli_fetch_array($cab)){
                          $lokasi=$cb['nama'];
                        }
                      }*/
                        ?>
            <tr>
                <td style="width:10%;" class="mono"><b><?php echo $no; ?></b></td>
                <td style="text-align:left; padding-left: 3px;" class="mono"><b><?php echo $data1['kd_produk']; ?></b></td>
                <td style="text-align:left; padding-left: 3px;" class="mono"><b><?php echo $data1['nama_produk']; ?></b></td>
                <td style="text-align:left; padding-left: 3px;" class="mono"><b><?php echo $data1['nama']; ?></b></td>
                <td style="text-align:left; padding-left: 3px;" class="mono"><b><?php echo $data1['alamat']; ?></b></td>
            </tr>         

             <?php   
              
                      $no++;} 
              ?>
        </table>
        </div>
    </div>
    <br />
    </div>
     
     <?php

$html = ob_get_contents(); //Proses untuk mengambil data
ob_end_clean();
//Here convert the encode for UTF-8, if you prefer the ISO-8859-1 just change for $mpdf->WriteHTML($html);
$mpdf->WriteHTML(utf8_encode($html));
// LOAD a stylesheet
$stylesheet = file_get_contents('mpdfstyletables.css');
$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

$mpdf->WriteHTML($html,1);

$mpdf->Output($nama_file."-".date("Y/m/d H:i:s").".pdf" ,'I');

 


exit; 
?>
</body>
</html>