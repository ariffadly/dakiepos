<?php 
session_start();
if (empty($_SESSION['username'])){
	header('location:../index.php');	
} else {
	include "../conn.php";
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>POS (Point Of Sales) V 1.0</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="../css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="../plugins/iCheck/flat/blue.css">
    <!-- Morris chart -->
    <link rel="stylesheet" href="../plugins/morris/morris.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="../plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="../plugins/datepicker/datepicker3.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="../plugins/daterangepicker/daterangepicker-bs3.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <?php include "header.php"; ?>
      <!-- Left side column. contains the logo and sidebar -->
      <?php include "menu.php"; ?>

<?php } ?>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Laporan
            <small>Front Coverr</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Laporan Penjualan</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Main row -->
          <div class="row">
            <!-- Left col -->
            <section class="col-lg-12 connectedSortable">

              <!-- TO DO List -->
              <div class="box box-primary">
                <div class="box-header">
                  <i class="ion ion-clipboard"></i>
                  <h3 class="box-title">Laporan Penjualan</h3>
                  <div class="box-tools pull-right">
                  
                  </div> 
                </div><!-- /.box-header -->
            
                <div class="box-body">
                <div class="col-lg-12">
              <form action='laporan-penjualan.php' method="POST">
            <div class="col-lg-3">
           <input type='text'  class="input-group date form-control" data-date="" data-date-format="yyyy-mm-dd" style="margin-bottom: 4px;" name='tglawal' placeholder='Cari dari tanggal' required="true" />  </div> <div class="col-lg-3"> <input type='text' class="input-group date form-control" data-date="" data-date-format="yyyy-mm-dd" style="margin-bottom: 4px;" name='tglakhir' placeholder='Sampai Tanggal'  required="true" /> </div>

                          <div class="col-lg-3">
                              <select name="lok" class="input-group form-control">
                              <option value="0"> -- Pilih Lokasi -- </option>
                              <?php
                    $quer="select * from cabang order by id";
                    $tamp=mysqli_query($koneksi, $quer) or die(mysqli_error());
                    while($dat=mysqli_fetch_array($tamp))
                    {
                    ?>
                              
                                  
              
              <option focus value="<?php echo $dat['ID']; ?>"><?php echo $dat['nama'];?></option>
                <?php } ?>
                              
                              </select>
                              </div>
           <input type='submit' value='Cari Data' class="btn btn-sm btn-primary" /> <a href='laporan-penjualan.php' class="btn btn-sm btn-success" > Refresh</a>
           </form>
          	</div><br />
                 <div class="print-area table-responsif" id="print-area-2">
                
                    <?php
                    
                    $query1="SELECT DISTINCT d.no_trans,t.tanggal_trans,p.nama_produk,p.harga_jual,d.qty,(p.harga_jual*d.qty) total,((p.harga_jual-p.harga_beli)*d.qty) profit,t.id_cabang FROM `detail_transaksi` d,transaksi t,produk p where d.no_trans=t.no_trans and d.kd_produk=p.kd_produk ORDER BY t.tanggal_trans DESC LIMIT 100";
                    if(isset($_POST['lok']) && isset($_POST['tglawal']) && isset($_POST['tglakhir'])){

                      $lok = $_POST['lok'];
                 $tglawal=$_POST['tglawal'];
                   $tglakhir=$_POST['tglakhir'];

                      if($_POST['lok']>0){
                      
                 $query1="SELECT DISTINCT d.no_trans,t.tanggal_trans,p.nama_produk,p.harga_jual,d.qty,(p.harga_jual*d.qty) total,((p.harga_jual-p.harga_beli)*d.qty) profit,t.id_cabang FROM `detail_transaksi` d,transaksi t,produk p 
                 where t.id_cabang=$lok and (t.tanggal_trans between '$tglawal'
                 and '$tglakhir') and d.no_trans=t.no_trans and d.kd_produk=p.kd_produk";
                      }else{
                        $query1="SELECT DISTINCT d.no_trans,t.tanggal_trans,p.nama_produk,p.harga_jual,d.qty,(p.harga_jual*d.qty) total,((p.harga_jual-p.harga_beli)*d.qty) profit,t.id_cabang FROM `detail_transaksi` d,transaksi t,produk p 
                 where (t.tanggal_trans between '$tglawal'
                 and '$tglakhir') and d.no_trans=t.no_trans and d.kd_produk=p.kd_produk";
                      }
                   
                 }
               
                    $tampil=mysqli_query($koneksi, $query1) or die(mysqli_error());
                    ?>
                  <table style="margin-top: 20px;" id="example" class="table table-hover table-bordered">
                  <thead>
                      <tr>
                        <th>NO</th>
                        <th>No Trans</th>
                        <th>Tanggal Transaksi</th>
                        <th>Nama Produk</th>
                        <th>Harga Jual</th>
                        <th>Terjual</th>
                        <th>Total</th>
                        <th>Profit</th>
                        <th>Lokasi</th>
                      </tr>
                  </thead>
                     <?php 
                     $no=0;
                     $temp="";
                     $profit=0;
                     while($data=mysqli_fetch_array($tampil))
                    { $trans=$data['no_trans'];
                      $bl = false;
                      if($temp<>$trans){
                      $no++;
                      $temp=$trans;
                      $bl=true;
                    }
                    $profit=$profit+$data['profit'];
                      $lokasi="";
                      $idcab=$data['id_cabang'];
                      if($idcab>0){
                        $cab=mysqli_query($koneksi,"SELECT * from cabang where ID=$idcab");
                        while($cb=mysqli_fetch_array($cab)){
                          $lokasi=$cb['nama'];
                        }
                      }
                     ?>
                    <tbody>
                    <tr>
                    <?php if($bl){ ?>
                    <td><center><?php echo $no; ?></center></td>
                    <td><?php echo $data['no_trans']; ?></td>
                    <td><?php echo $data['tanggal_trans']; ?></td>
                    <?php } else{ ?>
                    <td></td>
                    <td></td>
                    <td></td>
                    <?php } ?>
                    <td><?php echo $data['nama_produk']; ?></td>
                    <td>Rp. <?php echo number_format($data['harga_jual'],0,",","."); ?></td>
                    <td><?php echo $data['qty']; ?></td>
                     <td>Rp. <?php echo number_format($data['total'],0,",","."); ?></td>
                    <td>Rp. <?php echo number_format($data['profit'],0,",","."); ?></td>
                    <td><?php echo $lokasi; ?></td>
                    <!-- <td><center><div id="thanks"><a class="btn btn-sm btn-warning" data-placement="bottom" data-toggle="tooltip" title="Cetak Gaji" href="cetak-per-perusahaan.php?hal=edit&kd=<?php //echo $data['no_lhv'];?>"><span class="glyphicon glyphicon-print"></span></a></td></tr></div> -->
                    
                 <?php   
              } 
              ?>
              <tr>
                    <td colspan="7"><center><h5><b>Total Profit</b></h5></center></td>
                     <?php
    /*                 $query2="SELECT SUM(profit) AS harga from transaksi ";
                    
  if(isset($_POST['tglawal']) && isset($_POST['tglakhir']) && isset($_POST['lok'])){
                 $tglawal=$_POST['tglawal'];
                   $tglakhir=$_POST['tglakhir'];
                   $lok=$_POST['lok'];
                   if($lok<>"" && $lok<>NULL){  


                 $query2="SELECT SUM(profit) AS harga FROM transaksi
                 where id_cabang = $lok and (tanggal_trans between '$tglawal'
                 and '$tglakhir')";
               }else{
                $query2="SELECT SUM(profit) AS harga FROM transaksi
                 where (tanggal_trans between '$tglawal'
                 and '$tglakhir')";
               }
                   }
                    
                      $sql1 = mysqli_query($koneksi, $query2); 
             if(mysqli_num_rows($sql1) == 0){
				header("Location: laporan-penjualan.php");
			}else{
				$row1 = mysqli_fetch_assoc($sql1);
			}*/
            ?>
                    <td colspan="1"><h5><b>Rp. <?php echo number_format($profit,0,",","."); ?></b></h5></td>   
                    </tr>
                   </tbody> 
                   </table>
  </div>
   <iframe id="printing-frame" name="print_frame" src="about:blank" style="display:none;"></iframe>
   
    <div class="text-right">
                  <a href="cetak-penjualan.php?kd=<?php echo $_POST['tglawal'];?>&&kode=<?php echo $_POST['tglakhir'];?>&&idlok=<?php echo $_POST['lok'];?>" target="_blank" class="btn btn-sm btn-info">Export PDF  <i class="fa fa-download"></i></a>
                  <a href="javascript:printDiv('print-area-2');" class="btn btn-sm btn-danger" >Cetak  <i class="fa fa-print"></i></a>
              
                </div><br />
        
                </div><!-- /.box-body -->

              </div><!-- /.box -->

            </section><!-- /.Left col -->
          </div><!-- /.row (main row) -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <?php include "footer.php"; ?>

      <?php include "sidecontrol.php"; ?>
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->

 <script>
        function printDiv(elementId) {
    var a = document.getElementById('print-area-2').value;
    var b = document.getElementById(elementId).innerHTML;
    window.frames["print_frame"].document.title = document.title;
    window.frames["print_frame"].document.body.innerHTML = '<style>' + a + '</style>' + b;
    window.frames["print_frame"].window.focus();
    window.frames["print_frame"].window.print();
}
        </script>
    <!-- jQuery 2.1.4 -->
    <script src="../plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="../css/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.5 -->
    <script src="../bootstrap/js/bootstrap.min.js"></script>
    <!-- Morris.js charts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="../plugins/morris/morris.min.js"></script>
    <!-- Sparkline -->
    <script src="../plugins/sparkline/jquery.sparkline.min.js"></script>
    <!-- jvectormap -->
    <script src="../plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="../plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <!-- jQuery Knob Chart -->
    <script src="../plugins/knob/jquery.knob.js"></script>
    <!-- daterangepicker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src="../plugins/daterangepicker/daterangepicker.js"></script>
    <!-- datepicker -->
    <script src="../plugins/datepicker/bootstrap-datepicker.js"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <!-- Slimscroll -->
    <script src="../plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="../plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../dist/js/app.min.js"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="../dist/js/pages/dashboard.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../dist/js/demo.js"></script>
    <script>
	//options method for call datepicker
	$(".input-group.date").datepicker({ autoclose: true, todayHighlight: true });
	
    </script>
  </body>
</html>
