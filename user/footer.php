<footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>POS v</b> 1.1.0
        </div>
        <strong>Copyright &copy; 2017 <a href="http://dakie4cheese.com">DAKIE</a>.</strong> All rights reserved.
      </footer>