<?php
/* Database connection start */
/*$servername = "localhost";
$username = "root";
$password = "";
$dbname = "penjualan";

$koneksi = mysqli_connect($servername, $username, $password, $dbname) or die("Connection failed: " . mysqli_connect_error());

/* Database connection end */
include "../conn.php";

// storing  request (ie, get/post) global array to a variable  
$requestData= $_REQUEST;


$columns = array( 
// datatable column index  => database column name
	0 => 'id',
    1 => 'no_trans', 
	2 => 'tanggal_trans',
	3 => 'total',
    4 => 'profit'
    
);

// getting total number records without any search
$sql = "SELECT id, no_trans, tanggal_trans, total, profit";
$sql.=" FROM transaksi";
$query=mysqli_query($koneksi, $sql) or die("ajax-grid-transaksi.php: get Transaksi");
$totalData = mysqli_num_rows($query);
$totalFiltered = $totalData;  // when there is no search parameter then total number rows = total number filtered rows.


if( !empty($requestData['search']['value']) ) {
	// if there is a search parameter
	$sql = "SELECT id, no_trans, tanggal_trans, total, profit";
	$sql.=" FROM transaksi";
	$sql.=" WHERE id LIKE '".$requestData['search']['value']."%' ";    // $requestData['search']['value'] contains search parameter
	$sql.=" OR no_trans LIKE '".$requestData['search']['value']."%' ";
	$sql.=" OR tanggal_trans LIKE '".$requestData['search']['value']."%' ";
    $sql.=" OR total LIKE '".$requestData['search']['value']."%' ";
    $sql.=" OR profit LIKE '".$requestData['search']['value']."%' ";
	$query=mysqli_query($koneksi, $sql) or die("ajax-grid-transaksi.php: get Transaksi");
	$totalFiltered = mysqli_num_rows($query); // when there is a search parameter then we have to modify total number filtered rows as per search result without limit in the query 

	$sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."   LIMIT ".$requestData['start']." ,".$requestData['length']."   "; // $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc , $requestData['start'] contains start row number ,$requestData['length'] contains limit length.
	$query=mysqli_query($koneksi, $sql) or die("ajax-grid-transaksi.php: get Transaksi"); // again run query with limit
	
} else {	

	$sql = "SELECT id, no_trans, tanggal_trans, total, profit";
	$sql.=" FROM transaksi";
	$sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."   LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
	$query=mysqli_query($koneksi, $sql) or die("ajax-grid-transaksi.php: get Transaksi");   
	
}

$data = array();
while( $row=mysqli_fetch_array($query) ) {  // preparing an array
	$nestedData=array(); 

	$nestedData[] = $row["id"];
    $nestedData[] = $row["no_trans"];
	$nestedData[] = $row["tanggal_trans"];
	$nestedData[] = $row["total"];
    $nestedData[] = $row["profit"];
    $nestedData[] = '<td><center>
                     <a href="detail-transaksi.php?id='.$row['no_trans'].'"  data-toggle="tooltip" title="View Detail" class="btn btn-sm btn-success"> <i class="glyphicon glyphicon-search"></i> </a>
                    </center></td>';		
	
	$data[] = $nestedData;
    
}



$json_data = array(
			"draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
			"recordsTotal"    => intval( $totalData ),  // total number of records
			"recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data"            => $data   // total data array
			);

echo json_encode($json_data);  // send data as json format

?>
