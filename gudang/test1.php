<?php
//error_reporting(~E_WARNING & ~E_NOTICE);
require __DIR__ . '/autoload.php';
include "../conn.php";
$kembali = number_format($_GET['kembali']);
$bayar = number_format($_GET['bayar']);
$session = $_GET['session'];
$no_trans = $_GET['no_trans'];
use Mike42\Escpos\Printer;
//use Mike42\Escpos\EscposImage;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;

$connector = new WindowsPrintConnector("TM-P76xx");
$printer = new Printer($connector);

/* Information for the header receipt ------------------------------>> */
$query = mysqli_query($koneksi, "SELECT SUM(profit) AS pro, SUM(total) AS tot, no_trans, tanggal_trans FROM transaksi WHERE no_trans='$no_trans'");
            $data  = mysqli_fetch_array($query);
date_default_timezone_set("Asia/Bangkok");
date_default_timezone_get();
$date = date('l, jS F Y');
$time = date('h:i:s');

/* Start the printer */
//$logo = EscposImage::load("resources/frontcoverr.jpg", false);

/* Print top logo */
//$printer -> setJustification(Printer::JUSTIFY_CENTER);
//$printer -> graphics($logo);
// $printer -> bitImage($logo, Printer::IMG_DOUBLE_WIDTH);

/* Name of shop */
$printer -> selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
$printer -> setJustification(Printer::JUSTIFY_CENTER);
$printer -> setEmphasis(true);
$printer -> text("FRONTCOVERR\n");
$printer -> setEmphasis(false);
$printer -> selectPrintMode();
$printer -> text("Your Beauty Needs\n");
//$printer -> text("");
$printer -> setJustification(Printer::JUSTIFY_CENTER);
$printer -> text("===================================\n");

$printer -> text($date."\n");
$printer -> text($time."\n");
$printer -> setJustification(Printer::JUSTIFY_LEFT);

$printer -> text("Invoice : ".$no_trans . "\n");
$printer -> text("Cashier : ".$session . "\n");
$printer -> text("===================================");

$printer -> feed();

/* Title of receipt 
$printer -> setJustification(Printer::JUSTIFY_CENTER);
$printer -> setEmphasis(true);
$printer -> text("SALES INVOICE\n\n");
$printer -> setEmphasis(false);
*/

/* Items */
//information item in sales------------------------------------------>>
$query1="SELECT detail_transaksi.*, produk.* FROM detail_transaksi, produk 
    WHERE detail_transaksi.kd_produk=produk.kd_produk
    and detail_transaksi.no_trans='$no_trans'";
$tampil=mysqli_query($koneksi, $query1) or die(mysqli_error($koneksi));
            
$no=0;

while($data1=mysqli_fetch_array($tampil)){ 
    $no++;
    $a = $data1['harga_jual'];
    $b = $data1['qty'];
    $total = $a * $b;
    //$kodeproduk = $data1['kd_produk'];
    $namaproduk = $data1['nama_produk'];
    $hargajual = number_format($data1['harga_jual'],0,",",".");
    $qty = $data1['qty'];
    $subtotal = number_format($total,0,",",".");
    $totalx = number_format($data['tot'],0,",",".");


$printer -> setJustification(Printer::JUSTIFY_LEFT);
//$printer -> setEmphasis(true);
//$printer -> text($no. "\n");
$printer -> text($namaproduk."\n");
$printer -> setJustification(Printer::JUSTIFY_LEFT);
$printer -> text($qty . "x    "); 
$printer -> setJustification(Printer::JUSTIFY_CENTER);
$printer -> text($hargajual."    ");    
$printer -> setJustification(Printer::JUSTIFY_RIGHT);
$printer -> text($subtotal);

//$printer -> setEmphasis(true);
//$printer -> text($subtotal . "\n");
//$printer -> setEmphasis(false);
$printer -> feed();

}
/* Tax and total */
//$printer -> text($tax);
$printer -> selectPrintMode();
$printer -> text("-----------------------------------");
$printer -> setJustification(Printer::JUSTIFY_LEFT);
$printer -> text("Total    ");
$printer -> setJustification(Printer::JUSTIFY_RIGHT);
$printer -> text($totalx."\n");
$printer -> setJustification(Printer::JUSTIFY_LEFT);
$printer -> text("Bayar    ");
$printer -> setJustification(Printer::JUSTIFY_RIGHT);
$printer -> text($bayar."\n");
$printer -> setJustification(Printer::JUSTIFY_LEFT);
$printer -> text("Kembali  ");
$printer -> setJustification(Printer::JUSTIFY_RIGHT);
$printer -> text($kembali);

$printer -> selectPrintMode();

/* Footer */
$printer -> feed(2);
$printer -> setJustification(Printer::JUSTIFY_CENTER);
$printer -> setEmphasis(true);
$printer -> text("Barang yang sudah dibeli\n");
$printer -> text("tidak dapat ditukar/dikembalikan\n\n");
$printer -> setEmphasis(false);
$printer -> selectPrintMode(Printer::MODE_EMPHASIZED);
$printer -> text("Thank you for shopping \n");
$printer -> text("LINE@ dan IG : @frontcoverr\n");
$printer -> text("www.frontcoverr.com\n");
$printer -> text("\n\n\n\n");
$printer -> feed(2);

/* Cut the receipt and open the cash drawer */
$printer -> close();

header('Location: penjualan.php');