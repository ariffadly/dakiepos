<aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?php echo $_SESSION['gambar']; ?>" height="200" width="200" style="border: 2px solid white;" class="img-circle" alt="<?php echo $_SESSION['fullname']; ?>">
            </div>
            <div class="pull-left info">
              <p><?php echo $_SESSION['fullname']; ?></p>
              <a href="index.php"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div><br />
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MENU UTAMA PENJUALAN</li>
            
            <li class="treeview">
              <a href="penjualan.php">
                <i class="fa fa-dollar"></i> <span>Penjualan</span>
              </a>
                
            </li>
            
                        <li>
              <a href="#">
                <i class="fa fa-file"></i> <span>Produk</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="produk.php"><i class="fa fa-circle-o"></i> Produk</a></li>
                <li><a href="stok.php"><i class="fa fa-circle-o"></i> Tambah Stok</a></li>
                <li><a href="produk_importxls.php"><i class="fa fa-circle-o"></i> Import Stok</a></li>
                
                </ul>
            </li>
            <li class="treeview">
              <a href="cetak-penjualan.php" target="_blank">
                <i class="fa fa-dollar"></i> <span>Laporan Penjualan</span>
              </a>
                
            </li>
        </section>
        <!-- /.sidebar -->
      </aside>