<?php 
session_start();
if (empty($_SESSION['username'])){
	header('location:../index.php');	
} else {
	include "../conn.php";
date_default_timezone_set("Asia/Bangkok");
date_default_timezone_get();
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>POS (Point Of Sales) V 1.0</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../css/font-awesome.min.css">
    <link rel="stylesheet" href="../css/jquery-ui.min.css">
    <!-- Ionicons -->
    
    <link rel="stylesheet" href="../plugins/iCheck/all.css">
    
    <link rel="stylesheet" href="../css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="../plugins/iCheck/flat/blue.css">
    <!-- Morris chart -->
    <link rel="stylesheet" href="../plugins/morris/morris.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="../plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="../plugins/datepicker/datepicker3.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="../plugins/daterangepicker/daterangepicker-bs3.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <!-- css table datatables/dataTables -->
	<link rel="stylesheet" href="../plugins/datatables/dataTables.bootstrap.css"/>
    
    <link rel="stylesheet" href="../plugins/select2/select2.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <?php include "header.php"; ?>
      <!-- Left side column. contains the logo and sidebar -->
      <?php include "menu.php"; ?>

<?php
$timeout = 10; // Set timeout minutes
$logout_redirect_url = "../index.php"; // Set logout URL

?>
<?php } ?>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Penjualan
            <small>Point Of Sales</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">STOK</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Main row -->
          <div class="row">
            <!-- Left col -->
            <section class="col-lg-12 connectedSortable">

              <!-- TO DO List -->
              <div class="box box-primary">
                <div class="box-header">
                  <i class="ion ion-clipboard"></i>
                  <h3 class="box-title">Tambah stok</h3>
                  <div class="box-tools pull-right">
                  </div> 
                </div><!-- /.box-header -->
                
                <div class="box-body">
                <?php
                // 'hal']) == 'hapus'){
             if(isset($_GET['kd']) && isset($_GET['qty'])){
				$id = $_GET['kd'];
                $qty1 = $_GET['qty'];
				$cek = mysqli_query($koneksi, "SELECT * FROM stok_temporary WHERE kd_produk='$id' AND session ='$_SESSION[user_id]' AND id_cabang='$_SESSION[id_cabang]'");
				if(mysqli_num_rows($cek) == 0){
					echo "<script>window.location = 'stok.php'</script>";
                    //echo '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> Data tidak ditemukan.</div>';
				}else{
					$delete = mysqli_query($koneksi, "DELETE FROM stok_temporary WHERE kd_produk='$id' AND session ='$_SESSION[user_id]' AND id_cabang='$_SESSION[id_cabang]'");
                    
                    $que			= mysqli_query($koneksi, "UPDATE produk_cabang SET stock=(stock-'$qty1') WHERE kd_produk='$id' AND session ='$_SESSION[user_id]' AND id_cabang='$_SESSION[id_cabang]'");
					if($delete&&$que){
					echo "<script>window.location = 'stok.php'</script>";
                    	//echo '<div class="alert alert-primary alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> Data berhasil dihapus.</div>';
					}else{
						//echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> Data gagal dihapus.</div>';
					echo "<script>window.location = 'stok.php'</script>";
                    }
				}
			}
			?>
            
            <?php
            //input po transit dan tampil ke tabel
            if(isset($_POST['input'])){
                $qty = $_POST['qty'];
                $kd = $_POST['kode'];

                if(is_numeric($kd))  $sql = mysqli_query($koneksi, "SELECT * FROM produk WHERE kd_produk='$kd' and stock>0");
                else $sql = mysqli_query($koneksi, "SELECT * FROM produk WHERE nama_produk='$kd' and stock>0");
             
                if(mysqli_num_rows($sql) == 0){
				echo "<script>alert('Produk tidak terdaftar/stok kosong'); window.location = 'stok.php'</script>";
			}else{
				$row = mysqli_fetch_assoc($sql);
			
            
                $nama_produk = $row['nama_produk'];
                $harga_beli  = $row['harga_beli'];
                $harga_jual  = $row['harga_jual'];
                $stock       = $row['stock'];
                $kd = $row['kd_produk'];
                $tanggal = date("Y/m/d");
                
                $ck = mysqli_query($koneksi, "SELECT * FROM stok_temporary where kd_produk='$kd' AND session ='$_SESSION[user_id]' AND id_cabang=$_SESSION[id_cabang]");
                if(mysqli_num_rows($ck) == 0){

            $insert = mysqli_query($koneksi, "INSERT INTO stok_temporary(kd_produk, nama_produk, qty, session, id_cabang)
                                              VALUES('$kd', '$nama_produk', $qty, '$_SESSION[user_id]',$_SESSION[id_cabang])") or die(mysqli_error());
            }else{
            $insert = mysqli_query($koneksi, "UPDATE stok_temporary set qty=qty+$qty where kd_produk='$kd' AND session ='$_SESSION[user_id]' AND id_cabang=$_SESSION[id_cabang]") or die(mysqli_error());
            }
						
                        //$qu			= mysqli_query($koneksi, "UPDATE produk SET stock=(stock+'$qty') WHERE kd_produk='$kd'");
                        if($insert){
                          mysqli_query($koneksi, "INSERT INTO update_stock(kd_produk, nama_produk, qty, session,tgl,id_cabang)
                                              VALUES('$kd', '$nama_produk', $qty, '$_SESSION[username]','$tanggal',$_SESSION[id_cabang])") or die(mysqli_error());
						         echo "<script>window.location = 'stok.php'</script>";   
                                    //}
                        }else{
							echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Ups, Data Gagal Di simpan !</div>';
						}
          }
                    
			}
            ?>
   
           <?php
            $query5 = mysqli_query($koneksi, "SELECT * FROM stok_temporary WHERE session='$_SESSION[user_id]'");
            $data5  = mysqli_fetch_array($query5);
            ?>


                    
            <form id="form1" name="form1" method="post" action="">

                  <div class="col-md-1">
                  <div class="form-group">
                    <label></label>
                    <input type="text" class="form-control" name="qty" id="qty" value="1" autocomplete="off" required="required"  />
                  </div><!-- /.form-group -->
                  </div>
            <div class="col-md-6">
                  <div >
                    <label></label>
                   <input type="text" name="kode" class="form-control" id="kode" onkeypress="onEnter(event);" autofocus="true">
                  </div><!-- /.form-group -->
                  </div>
                  <div class="col-md-1">
                  <div class="form-group">
                    <label></label>
                    <input type="submit" class="btn btn btn-primary" name="input" id="inpu" value="Tambah" autocomplete="off" required="required"/>
                  </div><!-- /.form-group -->
                  </div>
            </form>
          
          
            
            <form id="formku" name="formku" method="post">
		 
            <br />
            <?php
                    $query1="select * from stok_temporary where session='$_SESSION[user_id]' ";
                    
                    $tampil=mysqli_query($koneksi, $query1) or die(mysqli_error());
                    ?>
        <div class="table table-responsive">
		<table name="table1" class="table table-hover table-bordered" id="table1">  
			<thead bgcolor="eeeeee" align="center">
            <tr>
            <th>No</th>
            <th>Kode</th>
            <th>Produk</th>
            <th>Jumlah</th>
            <th>Hapus</th>
			</thead>
            <?php 
                     $i=1;
                     while($data=mysqli_fetch_array($tampil))
                    { ?>
                    <tbody>
                    <tr>
                    <td><center><?php echo $i; ?></center></td>
                    <td><center><?php echo $data['kd_produk'];?></center></td>
                    <td><center><?php echo $data['nama_produk'];?></center></td>
                    <td><center><?php echo $data['qty'];?></center></td>
                    <td><center><div id="thanks"><a class="btn btn-sm btn-danger tooltips" data-placement="bottom" data-toggle="tooltip" title="Hapus List" href="stok.php?kd=<?php echo $data['kd_produk'];?>&&qty=<?php echo $data['qty'];?>"><span class="glyphicon glyphicon-trash"></a></center></td></tr></div>
                 <!--<td><center><div id="thanks"><a class="btn btn-sm btn-danger tooltips" data-placement="bottom" data-toggle="tooltip" title="Hapus List" href="stok.php?hal=hapus&&kd=<?php //echo $data['kd_produk'];?>&&qty=<?php //echo $data['qty'];?>"><span class="glyphicon glyphicon-trash"></a></center></td></tr></div>
                 -->
                 <?php   

                $tanggal = date("Y/m/d");
                 $b=$data['kd_produk'];
                 $c=$data['nama_produk'];
                 $f=$data['qty'];
                 
                if(isset($_POST['simpanpo'])){
				$kd_produk1	 = $b;
				$nama_produk1= $c;
                $qty1        = $f;
                
   
						$insert = mysqli_query($koneksi, "update produk set stock=stock-$qty1 where kd_produk='$kd_produk1'") or die(mysqli_error());
						if($insert){
                                   $delete = mysqli_query($koneksi, "DELETE FROM stok_temporary WHERE session='$_SESSION[user_id]' AND id_cabang=$_SESSION[id_cabang]");

                                   mysqli_query($koneksi, "DELETE FROM update_stock WHERE tgl!='$tanggal' AND session ='$_SESSION[user_id]' AND id_cabang=$_SESSION[id_cabang]");

                                   $pc = mysqli_query($koneksi, "Select * from produk_cabang where kd_produk='$kd_produk1'");

                                   if(mysqli_num_rows($pc)==0){
                                    mysqli_query($koneksi,"INSERT INTO produk_cabang(kd_produk,id_cabang,stock) values('$kd_produk1',$_SESSION[id_cabang],$qty1)");
                                   }else{
                                      mysqli_query($koneksi, "update produk_cabang set stock=stock+$qty1 where kd_produk='$kd_produk1'") or die(mysqli_error());            
                                   }

                                    echo "<script> 
                      
    
                              window.location = 'stok.php';
                              alert('Produk berhasil ditambah!');
                              </script>";
					                     
                        }else{
							echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Produk gagal disimpan !</div>';
						}
				//}else{
				//	echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>No PO Sudah Ada..!</div>';
				//}
                
			} ?>
            <?php  $i++;} 
              ?>
              
            </table>
            </div>
            	<div class="col-md-0">
					<button id="simpanpo" name="simpanpo" class="btn btn-success"><i class="fam-page-save"></i> Simpan Stok </button> 
				</div>
			</div> 
            </form>   
            </section><!-- /.Left col -->
          </div><!-- /.row (main row) -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <?php include "footer.php"; ?>

      <?php include "sidecontrol.php"; ?>
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    <script src="../plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <script src="../plugins/jQuery/utilities.js"></script>
    <script src="../plugins/jQueryUI/jquery-ui.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="../bootstrap/js/bootstrap.min.js"></script>
    <!-- DataTables -->
    <script src="../plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="../plugins/datatables/dataTables.bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="../plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="../plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../dist/js/demo.js"></script>
    <script src="../plugins/select2/select2.full.min.js"></script>
	  <!--<script type="text/javascript"> 

            $(function () {
                $("#lookup").dataTable({"lengthMenu":[25,50,75,100],"pageLength":25});
            });
  
   
        </script>-->
        <script type="text/javascript">
    function hitung() {
var total = document.formku.total.value;
var bayar = document.formku.bayar.value;
var kembali = document.formku.kembali.value;

kembali =  (bayar - total ) ;
document.formku.kembali.value = Math.floor( kembali);

}
function onEnter(e){
  var key=e.keyCode || e.which;
  
  if(key==13){

    //document.getElementById("qty").value=1;

  event.preventDefault();

setTimeout(function() {
    document.getElementById("inpu").click();
}, 1000);
                              
    
  }
/*  function submitForm(){
    document.form1.submit();
    document.form1.method='post';
}*/
}
</script>
        
       
 <script>
        $(document).ready(function() {
				var dataTable = $('#lookup').DataTable( {
					"processing": true,
					"serverSide": true,
					"ajax":{
						url :"ajaxin-grid-data.php", // json datasource
						type: "post",  // method  , by default get
						error: function(){  // error handling
							$(".lookup-error").html("");
							$("#lookup").append('<tbody class="employee-grid-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');
							$("#lookup_processing").css("display","none");
							
						}
					}
				} );
			} );
        </script>
        <script type="text/javascript">
$(function() 
{
 $("#kode").autocomplete({
  source: 'autocomplete.php'
 });
});
</script>
  </body>
</html>
